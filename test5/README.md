# 实验5：包，过程，函数的用法

## 实验目的

- 了解PL/SQL语言结构
- 了解PL/SQL变量和常量的声明和使用方法
- 学习包，过程，函数的用法。

## 实验内容

- 以hr用户登录

1. 创建一个包(Package)，包名是MyPack。
2. 在MyPack中创建一个函数Get_SalaryAmount,输入的参数是部门ID，通过查询员工表，统计每个部门的salay工资总额。
3. 在MyPack中创建一个过程GET_EMPLOYEES,输入参数是员工ID，在过程中使用游标，通过查询员工表，递归查询某个员工及其所有下属，子下属员工。
Oracle递归查询的语句格式是：

```sql
SELECT LEVEL,EMPLOYEE_ID,FIRST_NAME,MANAGER_ID FROM employees 
START WITH EMPLOYEE_ID = V_EMPLOYEE_ID 
CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID
```
![](step5.png)
## 实验注意事项

- 请按时完成实验，过时扣分。
- 查询语句及分析文档`必须提交`到：你的Oracle项目中的test5目录中。
- 上交后，通过这个地址应该可以打开你的源码：https://github.com/你的用户名/oracle/tree/master/test5
- 实验分析及结果文档说明书用Markdown格式编写。

## 脚本代码参考

```sql
create or replace PACKAGE MyPack IS
/*
本实验以实验4为基础。
包MyPack中有：
一个函数:Get_SalaryAmount(V_DEPARTMENT_ID NUMBER)，
一个过程:Get_Employees(V_EMPLOYEE_ID NUMBER)
*/
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER;
PROCEDURE Get_Employees(V_EMPLOYEE_ID NUMBER);
END MyPack;
/
create or replace PACKAGE BODY MyPack IS
FUNCTION Get_SalaryAmount(V_DEPARTMENT_ID NUMBER) RETURN NUMBER
AS
    N NUMBER(20,2); --注意，订单ORDERS.TRADE_RECEIVABLE的类型是NUMBER(8,2),汇总之后，数据要大得多。
    BEGIN
    SELECT SUM(salary) into N  FROM EMPLOYEES E
    WHERE E.DEPARTMENT_ID =V_DEPARTMENT_ID;
    RETURN N;
    END;

PROCEDURE GET_EMPLOYEES(V_EMPLOYEE_ID NUMBER)
AS
    LEFTSPACE VARCHAR(2000);
    begin
    --通过LEVEL判断递归的级别
    LEFTSPACE:=' ';
    --使用游标
    for v in
        (SELECT LEVEL,EMPLOYEE_ID, FIRST_NAME,MANAGER_ID FROM employees
        START WITH EMPLOYEE_ID = V_EMPLOYEE_ID
        CONNECT BY PRIOR EMPLOYEE_ID = MANAGER_ID)
    LOOP
        DBMS_OUTPUT.PUT_LINE(LPAD(LEFTSPACE,(V.LEVEL-1)*4,' ')||
                            V.EMPLOYEE_ID||' '||v.FIRST_NAME);
    END LOOP;
    END;
END MyPack;
/
```
![](step1.png)
![](step2.png)
## 测试



函数Get_SalaryAmount()测试方法：
```sh
select department_id,department_name,MyPack.Get_SalaryAmount(department_id) AS salary_total from departments;
```
输出：
![](step3.png)

过程Get_Employees()测试代码：
```
set serveroutput on
DECLARE
V_EMPLOYEE_ID NUMBER;    
BEGIN
V_EMPLOYEE_ID := 101;
MYPACK.Get_Employees (  V_EMPLOYEE_ID => V_EMPLOYEE_ID) ;    
END;
/
```
输出：
![](step4.png)


## 实验总结
本次实验的主题是PL/SQL编程语言的使用。在这个实验中，我学习了如何使用PL/SQL创建包、函数和过程。以下是我从本次实验中学到的一些重要知识点：

PL/SQL是一种存储过程语言，它是Oracle数据库系统中的一个重要组成部分。它允许开发人员在数据库中创建程序单元，这些程序单元可以在数据库服务器上执行。

PL/SQL包是一个包含一组相关程序单元的对象。这些程序单元可以是函数、过程、游标、变量等。包的主要目的是提供一种组织代码和控制访问的方式。

PL/SQL函数是一种返回值的程序单元，它接受一个或多个参数，并根据这些参数执行一些操作。函数通常用于计算某些值，并将计算结果返回给调用程序。

PL/SQL过程是一种没有返回值的程序单元，它也接受一个或多个参数，并根据这些参数执行一些操作。过程通常用于执行一些任务，例如插入或更新数据，而不需要返回任何结果。

在PL/SQL中，可以使用游标来遍历结果集。游标是一种可以迭代结果集中的行的对象，可以使用它来处理结果集中的每一行。

通过本次实验，我学习了如何使用PL/SQL创建包、函数和过程，并使用游标来遍历结果集。这些技能可以帮助我在以后的工作中更有效地使用Oracle数据库系统。




