# 实验4：PL/SQL语言打印杨辉三角

## 实验目的

掌握Oracle PL/SQL语言以及存储过程的编写。

## 实验内容

- 认真阅读并运行下面的杨辉三角源代码。
- 将源代码转为hr用户下的一个存储过程Procedure，名称为YHTriange，存储起来。存储过程接受行数N作为参数。
- 运行这个存储过程即可以打印出N行杨辉三角。
- 写出创建YHTriange的SQL语句。

## 杨辉三角源代码

```sql
set serveroutput on;
declare
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
N integer := 9; -- 一共打印9行数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
/
```
![](step1.png)
## 创建杨辉三角存储过程
```sql
create or replace PROCEDURE YHTriangle(N IN integer)
AS
type t_number is varray (100) of integer not null; --数组
i integer;
j integer;
spaces varchar2(30) :='   '; --三个空格，用于打印时分隔数字
rowArray t_number := t_number();
begin
    dbms_output.put_line('1'); --先打印第1行
    dbms_output.put(rpad(1,9,' '));--先打印第2行
    dbms_output.put(rpad(1,9,' '));--打印第一个1
    dbms_output.put_line(''); --打印换行
    --初始化数组数据
    for i in 1 .. N loop
        rowArray.extend;
    end loop;
    rowArray(1):=1;
    rowArray(2):=1;    
    for i in 3 .. N --打印每行，从第3行起
    loop
        rowArray(i):=1;    
        j:=i-1;
            --准备第j行的数组数据，第j行数据有j个数字，第1和第j个数字为1
            --这里从第j-1个数字循环到第2个数字，顺序是从右到左
        while j>1 
        loop
            rowArray(j):=rowArray(j)+rowArray(j-1);
            j:=j-1;
        end loop;
            --打印第i行
        for j in 1 .. i
        loop
            dbms_output.put(rpad(rowArray(j),9,' '));--打印第一个1
        end loop;
        dbms_output.put_line(''); --打印换行
    end loop;
END;
```
该存储过程使用了VARRAY类型定义了一个名为t_number的数组类型，该类型包含100个整数元素。存储过程在开始时初始化了数组，并依次计算每一行的数据，然后将它们打印出来。

首先，存储过程打印出第一行的数字1，并在第二行打印两个数字1。然后，在循环中，存储过程计算并打印出从第三行到第N行的数据，每行数据的数字个数与行数相同。在计算每行数据时，它们是根据前一行的数据计算出来的，通过迭代计算每个数字。最后，存储过程将每行数据打印到屏幕上，以形成杨辉三角形的形状。

请注意，此存储过程使用了PL/SQL中的dbms_output包来在控制台上打印输出结果。如果想要在其他地方使用这些数据，需要对此存储过程进行修改，将输出结果传递给其他程序或存储到数据库表中。
![](step2.png)
![](step3.png)
## 打印杨辉三角
```sql
set SERVEROUTPUT on;
begin
YHTriangle(20);
end;
```
![](step4.png)
## 实验总结
本次实验的任务是将杨辉三角的生成代码转换为存储过程，并在 HR 用户下创建这个存储过程。具体而言，我们需要实现一个名为 YHTriangle 的存储过程，接受一个整数参数 N，并打印出 N 行杨辉三角。
我们首先对杨辉三角的生成代码进行了修改，使其适用于存储过程。在存储过程中，我们使用了一个名为 t_number 的 Varray 类型来存储每一行的数字，使用 dbms_output.put_line 和 dbms_output.put 方法将数字打印到控制台上。在初始化数组和生成每一行数字时，我们使用了循环结构和条件语句等基本的编程概念。
接着，我们使用了 SQL 语句创建了名为 YHTriangle 的存储过程，使用 create or replace procedure 语句创建一个存储过程，指定输入参数和过程体，并在过程体内实现杨辉三角的生成和打印。
最后，我们在 SQLPlus 中测试了 YHTriangle 存储过程的功能。我们输入了一个整数参数，调用存储过程并打印出杨辉三角的前 N 行。在测试中，我们发现存储过程能够正确地打印出杨辉三角，并且能够处理输入参数为较大数值的情况。
总的来说，本次实验让我进一步熟悉了 PL/SQL 的编写和存储过程的创建方法，同时也加深了我对基本编程概念的理解。通过实践，我深刻体会到了编程的乐趣和挑战，也认识到了不断学习和实践的重要性。
